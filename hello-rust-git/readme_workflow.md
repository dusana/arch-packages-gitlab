### Izdelava arch paketa za najbolj preprost rust hello world program  

Narejena PKGBUILD datoteka je v tem direktoriju. Sledijo koraki kako jo narediti.   

### Workflow  

    cd $HOME/abs  
    cp /usr/share/pacman/PKGBUILD.proto .  
    mv PKGBUILD.proto PKGBUILD  

Vsebina končane PKGBUILD datoteke je v naslednjih vrsticah. V njej je tudi vrstica, ki potegne izvorno kodo iz git repozitorija. Ko PKGBUILD datoteke še nimaš, v $HOME/abs direktorij preslikaj git repozitorij.  


### Zunanje povezave  

[arch wiki rust package guide](https://wiki.archlinux.org/index.php/Rust_package_guidelines)   
[primer PKGBUILD za cbindgen](https://git.archlinux.org/svntogit/packages.git/tree/trunk/PKGBUILD?h=packages/cbindgen)    
[primer PKGBUILD za ripgrep](https://git.archlinux.org/svntogit/community.git/tree/trunk/PKGBUILD?h=packages/ripgrep)  
Zvezek 100  
