### Vodnik  

Delal sem po tem vodniku. Imaš tudi v zvezku 22 str.9  
[thoughtbot.com](https://thoughtbot.com/blog/the-magic-behind-configure-make-make-install) 

Zaradi preglednosti sem vse, kar naredijo autotools, premaknil v poddirektorij work. V tem direktoriju so samo datoteke, ki jih moraš narediti ti. Ko boš delal ponovno, naredi nov tmp direktorij in izvedi vse te operacije v njem. V ta tmp direktorij skopiraj samo datoteke iz tega direktorija.  

### Workflow  

    mkdir -p $HOME/tmp4  
    cp configure.ac main.c Makefile.am $HOME/tmp4  
    cd $HOME/tmp4  

1. configure.ac  

    AC_INIT([helloworld],[0.1],[dusan.alic@gmail.com])    
    AM_INIT_AUTOMAKE  
    AC_PROG_CC  
    AC_CONFIG_FILE([Makefile])  
    AC_OUTPUT   

2. Makefile.am  

    AUTOMAKE_OPTIONS = foreign  
    bin_PROGRAMS = helloworld  
    helloworld_SOURCES = main.c  


3. main.c  

    #include <stdio.h>  
    #include <stdlib.h>  

    int main (int argc, char* argv[])  {

      system("clear");  
      printf("%s\n", "Hello World");  
      return 0;
    }  

4. Nato izvedi ukaze  

    aclocal                   #Pripravi okolje                     
    autoconf                  #Izdelaj skript configure iz datoteke configure.ac  
    automake --add-missing    # Izdelaj datoteko Makefile.in iz datoteke Makefile.am  
    ./configure               # Izdelaj datoteko Makefile iz datoteke Makefile.in  
    make distcheck            # Uporabi Makefile, da izdelaš in testiraš tarball datoteko za distribucijo.      

5. Naredi se datoteka helloworld-0.1.tar.gz, ki jo lahko instaliraš  ali pa nadalje uporabiš za pripravo arch paketa.  

    ./configure  
    ./make  
    sudo make install   


### Izdelava arch paketa  

    mkdir -p $HOME/abs  
    cp helloworld-0.1.tar.gz $HOME/abs 

Če boš tarball datoteko gostil na web strežniku, potem v PKGBUILD nastavi web pot do te datoteke. Npr. http://dusana.net/packages/helloworld-0.1.tar.gz.   

Na rpbi skopiraj helloworld-0.1.tar.gz v $HOME/tmp direktorij.  

    cd $HOME/tmp  (rbpi)    
    busybox httpd -f -p 8000  (don't daemonize)  

Sedaj je datoteka na voljo za prenos z wget, curl ali browserjem. Oziroma, to nastaviš v PKGBUILD.   
Ko to fazo stestiraš (preneseš tarball source z oddaljenega serverja), nadaljuj lokalno.   

    cp /usr/share/pacman/PKGBUILD.proto .  (si v $HOME/abs)  
    mv PKGBUILD.proto PKGBUILD  

Datoteko PKGBUILD imaš v $HOME/projects/arch-packages/c/. Na tem mestu samo glavni poudarki:  


    # Maintainer Dušan Alič <dusan.alic@domain.com>    
    pkgname=helloworld  
    pkgver=0.1  
    pkgrel=1  
    pkgdesc="Najbolj osnovni c program za namestitev"  
    arch=(x86_64)  
    url="http://rbpi:8000/helloworld-0.1.tar.gz"  
    license=('GPL')  
    source=("$url")  

    prepare() {  
      cd "$pkgname-pkgver"  

    }

    build() {  
     cd "$pkgname-$pkgver"  
     ./configure --prefix=/usr  
     make  
    }

    check() {  
      cd "$pkgname-$pkgver"  
      make -k check  

    }

    package() {  
      cd "$pkgname-$pkgver"  
      make DESTDIR="$pkgdir/" install  

    }
  
Na konec datoteke sam doda md5sum.


In potem glavni trik!! :  

    makepkg -g >> PKGBUILD && makepkg (ker package ne upošteva git packaging guidelines...)

[allan](https://bbs.archlinux.org/viewtopic.php?id=77857)  


### Namestitev  

    sudo pacman -U helloworld-0.1-1-x86_64.pkg.tar.xz  
