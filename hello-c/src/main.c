/*
 * =====================================================================================
 *
 *       Filename:  hello.c
 *
 *    Description:  Testiranje compile za synology nas
 *
 *        Version:  1.0
 *        Created:  06/04/2016 03:53:13 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Dusan Alic (), 
 *   Organization:  
 *
 * =====================================================================================
 */ 
/*POZOR: Med #include in <stdio.h> NE SME biti presledka, ker sicer ne deluje na debianu v pch-c200 */
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char* argv[])
{
	system("clear");
	printf("%s\n","Hello, World");
	return 0;
}

