# README #

Uporaba ABS (Arch Build System) za izdelavo namestitvenih programov.

### What is this repository for? ###

* Uporabljaj hlparch.zsh abs. Tam je podroben opis izdelave PKGBUILD, lastnega repozitorija...


### Workflow  

    mkdir $HOME/abs  
    cd $HOME/abs  

### Datoteke  

    /etc/makepkg.conf  (del pacman paketa)  
    /usr/share/pacman/PKGBUILD.proto  

### Orodja (tools,Tools)  

1. basel-devel.   
2. asp  
3. pkgfile  
4. paclist  
5. namcap  
  
     sudo pacman -Syu base-devel  
     sudo pacman -Syu asp  
     sudo pacman -Syu pkgfile  
     sudo pacman -Syu pacman-contrib (za paclist)  
     sudo pacman -Syu namcap  



### Zunanje povezave  

[arch wiki package standards](https://wiki.archlinux.org/index.php/Arch_package_guidelines)  
zvezek 65  
